"use strict";

var LENIENT_PROPERTIES = ["entries", "sort", "values"];
var STRICT_PROPERTIES = ["append", "delete", "get", "getAll", "has", "keys", "set" // "toString" excluded because Object::toString exists
];
module.exports = {
  LENIENT_PROPERTIES: LENIENT_PROPERTIES,
  STRICT_PROPERTIES: STRICT_PROPERTIES
};
//# sourceMappingURL=props.js.map