"use strict";

var _require = require("has-to-string-tag-x"),
    hasToStringTag = _require["default"];

var isObject = require("is-object");

var _require2 = require("./props"),
    LENIENT_PROPERTIES = _require2.LENIENT_PROPERTIES,
    STRICT_PROPERTIES = _require2.STRICT_PROPERTIES;

var SEARCH_PARAMS_CLASS = "[object URLSearchParams]";
var toStringTag = Object.prototype.toString;

var isURLSearchParams = function isURLSearchParams(searchParams) {
  var supportIncomplete = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (!isObject(searchParams)) {
    return false;
  } else if (hasToStringTag && toStringTag.call(searchParams) !== SEARCH_PARAMS_CLASS) {
    // Shimmed implementation with incorrect constructor name
    return false;
  } else if (!STRICT_PROPERTIES.every(function (prop) {
    return prop in searchParams;
  })) {
    return false;
  } else if (supportIncomplete) {
    return true;
  } else {
    return LENIENT_PROPERTIES.every(function (prop) {
      return prop in searchParams;
    });
  }
};

isURLSearchParams.lenient = function (searchParams) {
  return isURLSearchParams(searchParams, true);
};

module.exports = Object.freeze(isURLSearchParams);
//# sourceMappingURL=index.js.map