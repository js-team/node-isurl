"use strict";

var _require = require("has-to-string-tag-x"),
    hasToStringTag = _require["default"];

var isObject = require("is-object");

var isURLSearchParams = require("is-urlsearchparams");

var _require2 = require("./props"),
    LENIENT_PROPERTIES = _require2.LENIENT_PROPERTIES,
    STRICT_PROPERTIES = _require2.STRICT_PROPERTIES;

var SEARCH_PARAMS = "searchParams";
var URL_CLASS = "[object URL]";
var toStringTag = Object.prototype.toString;

var isURL = function isURL(url) {
  var supportIncomplete = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (!isObject(url)) {
    return false;
  } else if (hasToStringTag && toStringTag.call(url) !== URL_CLASS) {
    // Shimmed implementation with incorrect constructor name
    return false;
  } else if (!STRICT_PROPERTIES.every(function (prop) {
    return prop in url;
  })) {
    return false;
  } else if (supportIncomplete && SEARCH_PARAMS in url) {
    return isURLSearchParams.lenient(url.searchParams);
  } else if (supportIncomplete) {
    return true;
  } else if (LENIENT_PROPERTIES.every(function (prop) {
    return prop in url;
  })) {
    return isURLSearchParams(url.searchParams);
  } else {
    return false;
  }
};

isURL.lenient = function (url) {
  return isURL(url, true);
};

module.exports = Object.freeze(isURL);
//# sourceMappingURL=index.js.map