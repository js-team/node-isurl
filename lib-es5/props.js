"use strict";

var LENIENT_PROPERTIES = ["origin", "searchParams", "toJSON"];
var STRICT_PROPERTIES = ["hash", "host", "hostname", "href", "password", "pathname", "port", "protocol", "search", // "toString" excluded because Object::toString exists
"username"];
module.exports = {
  LENIENT_PROPERTIES: LENIENT_PROPERTIES,
  STRICT_PROPERTIES: STRICT_PROPERTIES
};
//# sourceMappingURL=props.js.map